<?php

namespace Drupal\noahs_page_builder\Form;

use Drupal\commerce_product\Entity\ProductType;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\noahs_page_builder\Controller\NoahsController;
use Drupal\noahs_page_builder\Service\ControlServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\noahs_page_builder\Fonts;
use Drupal\editor\Entity\Editor;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Defines a form that configures noahs_page_builder settings.
 */
class NoahsSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * Control services.
   *
   * @var \Drupal\noahs_page_builder\Service\ControlServices
   */
  protected $controlService;

  /**
   * NoahsSaveStylesController constructor.
   */
  public function __construct(ControlServices $control_service) {
    $this->controlService = $control_service;
  }

  /**
   * Creates an instance of the controller.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('noahs_page_builder.control_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'noahs_page_builder_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['noahs_page_builder.settings'];
  }

  /**
   * {@inheritdoc}
   */
  private $fonts = FALSE;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $settings = $this->config('noahs_page_builder.settings');

    $querys = $this->controlService->getMediaQuery();

    $form['#attached']['library'][] = 'noahs_page_builder/noahs_page_builder.assets.settings';
    $pallete_color = [];
    $pallete_color[] = $settings->get('principal_color') ?? '#2389ab';
    $pallete_color[] = $settings->get('secondary_color') ?? '#4a4a4a';

    $custom_colors = $settings->get('custom_color') ?? [];

    foreach ($custom_colors as $color) {
      if (!empty($color['hex'])) {
        $pallete_color[] = $color['hex']; // Solo añadimos el HEX
      }
    }

    $form['#attached']['drupalSettings']['noahs_page_builder']['pallete_color'] = $pallete_color;

    $node_types = NodeType::loadMultiple();
    $node_types_options = [];

    foreach ($node_types as $node_type) {
      $node_types_options[$node_type->id()] = $node_type->label();
    }

    $vocabularies = Vocabulary::loadMultiple();

    $vbo_options = [];

    foreach ($vocabularies as $vbo) {
      $vbo_options[$vbo->id()] = $vbo->label();
    }

    $commerce_options = [];
    if (is_module_installed('commerce')) {
      $commerce_types = ProductType::loadMultiple();
      foreach ($commerce_types as $product_type) {
        $commerce_options[$product_type->id()] = $product_type->label();
      }
    }
    $editors = Editor::loadMultiple();

    $editors_options = [];

    foreach ($editors as $k => $editor_type) {
      $editors_options[$k] = $k;
    }
    
    $form['custom_link'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<a href=":url" class="button button--primary">Start creating general styles</a>', [
        ':url' => Url::fromRoute('noahs_page_builder.noahs_settings_styles', [], ['absolute' => TRUE])->toString(),
      ]),
      '#allowed_tags' => ['a'],
    ];

    $form['tabs'] = [
      '#type' => 'vertical_tabs',
    ];

    /* =========================   General  ========================= */
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#group' => 'tabs',
    ];

    $form['general']['develop_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Develop mode'),
      '#default_value' => $settings->get('develop_mode') ? $settings->get('develop_mode') : '',
      '#description' => $this->t("Not save css cache"),
    ];

    $form['general']['use_editor'] = [
      '#type' => 'select',
      '#title' => $this->t('Select your CKEDITOR'),
      '#options' => $editors_options,
      '#default_value' => $settings->get('use_editor') ?? [],
      '#required' => TRUE,
    ];

    $form['general']['use_in_ctype'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Use in content type'),
      '#options' => $node_types_options,
      '#default_value' => $settings->get('use_in_ctype') ?? [],
    ];

    $form['general']['use_in_vtype'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Use in Vocabulary type'),
      '#options' => $vbo_options,
      '#default_value' => $settings->get('use_in_vtype') ?? [],
    ];
    
    $form['general']['use_in_products'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Use in Products type'),
      '#options' => $commerce_options,
      '#default_value' => $settings->get('use_in_products') ?? [],
    ];

    /* =========================   Container  ========================= */
    $form['style'] = [
      '#type' => 'details',
      '#title' => $this->t('Container & breackpoints'),
      '#group' => 'tabs',
    ];

    $form['style']['container_width'] = [
      '#type' => 'number',
      '#title' => 'Content Width',
      '#default_value' => $settings->get('container_width') ?? '',
      '#placeholder' => 1320,
      '#field_suffix' => 'px',
      '#description' => $this->t("Sets the default width of the content area (Default: 1320px)"),
    ];

    $form['style']['viewport_tablet'] = [
      '#type' => 'number',
      '#title' => 'Tablet Breakpoint',
      '#default_value' => $settings->get('viewport_tablet') ?? 959,
      '#placeholder' => 959,
      '#field_suffix' => 'px',
      '#description' => $this->t("Sets the breakpoint to tablet devices (Default: 959px)."),
    ];

    $form['style']['viewport_mobile'] = [
      '#type' => 'number',
      '#title' => 'Mobile Breakpoint',
      '#default_value' => $settings->get('viewport_md') ?? 767,
      '#placeholder' => 767,
      '#field_suffix' => 'px',
      '#description' => $this->t("Sets the breakpoint to tablet devices (Default: 767px)."),
    ];

    /* =========================   Fonts  ========================= */
    $form['fonts'] = [
      '#type' => 'details',
      '#title' => $this->t('Fonts'),
      '#group' => 'tabs',
    ];

    $noahs_page_builder_fonts = Fonts::getFonts();
    $google_font_api = $settings->get('google_font_api') ? $settings->get('google_font_api') : '';
    $google_fonts = $this->getGoogleFonts($google_font_api);
    $fonts = array_merge($noahs_page_builder_fonts, $google_fonts);

    $form['fonts']['google_font_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Font API'),
      '#default_value' => $google_font_api,
      '#description' => $this->t("Paste here your google font api. <a href='https://developers.google.com/fonts/docs/developer_api' target='_blank'>Get your API here</a>"),
    ];

    $form['fonts']['heading_font'] = [
      '#type' => 'select',
      '#title' => $this->t('Headings front'),
      '#default_value' => $settings->get('heading_font') ?? 'Playfair Display',
      '#description' => $this->t("Set font to h1 - h2 - h3 - h4 - h5 - h6"),
      '#options' => $fonts,
      '#attributes' => ['class' => ['chosen-select']],
    ];

    $form['fonts']['general_font'] = [
      '#type' => 'select',
      '#title' => $this->t('General front'),
      '#default_value' => $settings->get('general_font') ?? 'Playfair Display',
      '#description' => $this->t("Set font to general body/html"),
      '#options' => $fonts,
      '#attributes' => ['class' => ['chosen-select']],
    ];


    /* =========================   Colors  ========================= */
            
    $form['colors'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Colors'),
      '#group' => 'tabs',
    ];
  
    $form['colors']['palette'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Colors'),
      '#group' => 'colors',
    ];
  
    $form['colors']['palette']['principal_color'] = [
      '#type' => 'textfield',
      '#title' => 'Principal Color',
      '#default_value' => $settings->get('principal_color') ?? '',
      '#description' => $this->t("Set Principal color"),
      '#attributes' => ['class' => ['form-control-color']],
    ];
  
    $form['colors']['palette']['secondary_color'] = [
      '#type' => 'textfield',
      '#title' => 'Secondary Color',
      '#default_value' => $settings->get('secondary_color') ?? '',
      '#description' => $this->t("Set Secondary color"),
      '#attributes' => ['class' => ['form-control-color']],
    ];
  
    // Contenedor para los colores dinámicos.
    $form['colors']['custom_color'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Colors'),
      '#prefix' => '<div id="color-palette-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    ];
  
    // Obtener colores desde el estado del formulario o configuración.


    $colors = $form_state->get('custom_color');
    if ($colors === NULL) {
      $colors = $settings->get('custom_color') ?? [];
      $form_state->set('custom_color', $colors);
    }

    // Agregar dinámicamente los colores guardados.
    foreach ($colors as $key => $color) {

      $form['colors']['custom_color'][$key] = [  // 👈 El índice debe estar directamente aquí
        '#type' => 'fieldset',
        '#attributes' => ['class' => ['noahs-custom-colors']],
      ];
    
      $form['colors']['custom_color'][$key]['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Color Name'),
        '#default_value' => $color['name'] ?? '',
        '#required' => TRUE,
      ];
    
      $form['colors']['custom_color'][$key]['hex'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Color'),
        '#default_value' => $color['hex'] ?? '',
        '#required' => TRUE,
        '#attributes' => ['class' => ['form-control-color']],
      ];
  
      $form['colors']['custom_color'][$key]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => 'remove_' . $key,
        '#submit' => ['::removeColorCallback'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::updateColorPalette',
          'wrapper' => 'color-palette-wrapper',
        ],
      ];
    }
  
    // Botón para agregar más colores
    $form['colors']['custom_color']['add_color'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Color'),
      '#submit' => ['::addColorCallback'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::updateColorPalette',
        'wrapper' => 'color-palette-wrapper',
      ],
    ];


    /* =========================   Custom Css  ========================= */

    $form['custom_css'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom Css'),
      '#attributes' => ['style' => ['margin-top:25px']],
    ];

    $form['custom_css']['noahs_page_builder_custom_css'] = [
      '#type' => 'textarea',
      '#rows' => 15,
      '#title' => $this->t('CSS Code'),
      '#default_value' => $settings->get('noahs_page_builder_custom_css'),
      '#description' => $this->t('Please enter custom style without <b> @style </b> tag.', ["@style" => '<style>']) ,
      '#attributes' => ['class' => ['noahs_page_builder_codemirror']],
    // '#group' => 'custom_css',
    ];
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * Callback para añadir un nuevo color.
   */
  public function addColorCallback(array &$form, FormStateInterface $form_state) {
    // Obtener los colores actuales desde el estado del formulario.
    $colors = $form_state->get('custom_color') ?? [];

    // Agregar un nuevo color vacío.
    $colors[] = ['name' => '', 'hex' => '#000000'];

    // Guardar la nueva lista en el estado del formulario.
    $form_state->set('custom_color', $colors);

    // Forzar la reconstrucción del formulario.
    $form_state->setRebuild(TRUE);
  }

  /**
   * Callback para eliminar un color específico.
   */
  public function removeColorCallback(array &$form, FormStateInterface $form_state) {
    // Obtener el botón que fue presionado.
    $triggering_element = $form_state->getTriggeringElement();
    
    // Extraer el índice del color a eliminar.
    if (preg_match('/remove_(\d+)/', $triggering_element['#name'], $matches)) {
      $index = (int) $matches[1];

      // Obtener la lista de colores actual del estado del formulario.
      $colors = $form_state->get('custom_color') ?? [];

      // Eliminar el color si el índice es válido.
      if (isset($colors[$index])) {
        unset($colors[$index]);
        $colors = array_values($colors); // Reindexar el array.
      }

      // Guardar la lista actualizada de colores en el estado del formulario.
      $form_state->set('custom_color', $colors);
      
      // Marcar el formulario para ser reconstruido.
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * Callback AJAX para actualizar la paleta de colores.
   */
  public function updateColorPalette(array &$form, FormStateInterface $form_state) {
    return $form['colors']['custom_color'];
  }

  /**
   * {@inheritdoc}
   */
  public function getGoogleFonts($api) {
    $options = [];
    $url = "https://www.googleapis.com/webfonts/v1/webfonts?key=" . $api;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);

    $result = json_decode($result, TRUE);

    if (!$result['error']) {
      foreach ($result['items'] as $v) {

        $options[$v['family']] = $v['family'];
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $fontName1 = str_replace(" ", "+", $form_state->getValue('heading_font') ?? '');
    $fontName2 = str_replace(" ", "+", $form_state->getValue('general_font') ?? '');
    $container = $form_state->getValue('container_width') . 'px' ?? 'none';
    if ($fontName1 === $fontName2) {
      $styles = "@import url('https://fonts.googleapis.com/css2?family=" . $fontName1 . "&display=swap');";
    }
    else {
      $styles = "@import url('https://fonts.googleapis.com/css2?family=" . $fontName1 . "&family=" . $fontName2 . "&display=swap');";
    }
    $styles .= ':root {
      --noahs_page_builder-principal-color: ' . $form_state->getValue('principal_color') . ';
      --noahs_page_builder-secondary-color: ' . $form_state->getValue('secondary_color') . '
    }';
    $styles .= 'body{font-family:"' . $form_state->getValue('general_font') . '";}';
    $styles .= '.container{max-width:' . $container . ';}';
    $styles .= 'h1, h2, h4, h4, h5, h6{';
    if (!empty($form_state->getValue('heading_color'))) {
      $styles .= 'color:' . $form_state->getValue('heading_color') . ';';
    }
    if (!empty($form_state->getValue('heading_font') || $form_state->getValue('general_font'))) {
      $styles .= 'font-family:"' . $form_state->getValue('heading_font') . '";' ?? $form_state->getValue('general_font') . '";';
    }

    $styles .= '}';  

    $styles .= $form_state->getValue('noahs_page_builder_custom_css');

    $theme = \Drupal::theme()->getActiveTheme()->getName();

    $noahsContrller = \Drupal::classResolver(NoahsController::class);
    $noahsContrller->saveCss('noahs_settings', 'noahs_general_settings', 'no_language', $styles);

    $form_state->cleanValues();
    $values = $form_state->getValues();
    $config = $this->config('noahs_page_builder.settings');

    foreach ($values as $key => $value) {
      if (is_array($value)) {
        // Si es un array, se asume que son valores anidados.
        foreach ($value as $sub_key => $sub_value) {
          $config->set("$key.$sub_key", $sub_value);
        }
      }
      else {
        // Si no es un array, se asume un campo simple.
        $config->set($key, $value);
      }
    }

    $config->save();

    drupal_flush_all_caches();

    // drupal_flush_all_caches();
    return parent::submitForm($form, $form_state);
  }

}
