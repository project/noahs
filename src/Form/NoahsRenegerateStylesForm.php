<?php

namespace Drupal\noahs_page_builder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\noahs_page_builder\Controller\NoahsSaveStylesController;

/**
 * Defines a form that configures noahs_page_builder settings.
 */
class NoahsRenegerateStylesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'noahs-regenerate-styles-form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['noahs_page_builder.regenerate_styels'];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state, $nid = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $elements = \Drupal::database()->select('noahs_page_builder_page', 'd')
      ->fields('d', ['entity_id', 'entity_type'])
      ->execute()
      ->fetchAll();

    foreach ($elements as $element) {
      if (empty($element->entity_type)) {

        if (strpos($element->entity_id, 'product_') !== FALSE) {
          $entity_id = str_replace('product_', '', $element->entity_id);
          $entity = \Drupal::entityTypeManager()->getStorage('commerce_product')->load($entity_id);
          \Drupal::database()->update("noahs_page_builder_page")
            ->fields([
              'noahs_id' => $entity->id(),
              'entity_type' => $entity->getEntityTypeId(),
              'entity_id' => $entity->id(),
            ])
            ->condition('entity_id', $element->entity_id)
            ->execute();

        }
        else {
          $entity = \Drupal::entityTypeManager()->getStorage('node')->load($element->entity_id);
          \Drupal::database()->update("noahs_page_builder_page")
            ->fields([
              'noahs_id' => $entity->id(),
              'entity_type' => $entity->getEntityTypeId(),
              'entity_id' => $entity->id(),
            ])
            ->condition('entity_id', $entity->id())
            ->execute();
        }
      }
    }

    $new_elements = \Drupal::database()->select('noahs_page_builder_page', 'd')
      ->fields('d', ['entity_id', 'entity_type'])
      ->execute()
      ->fetchAll();

    foreach ($new_elements as $element) {

      \Drupal::classResolver(NoahsSaveStylesController::class)->save($element->entity_type, $element->entity_id);
    }
    $pro_elements = \Drupal::database()->select('noahs_page_builder_pro_themes', 'd')
      ->fields('d')
      ->execute()
      ->fetchAll();

    foreach ($pro_elements as $element) {
  
      \Drupal::classResolver(NoahsSaveStylesController::class)->save('pro_theme', $element->type);
    }

  }

}
