<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Utility\Error;
use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Show data from noahs creator.
 */
class NoahsPageController extends ControllerBase {

  /**
   * The http connection.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  public function __construct(ClientInterface $httpClient) {
    $this->httpClient = $httpClient;
  }

  /**
   * NoahsController constructor.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * View data.
   */
  public static function view() {
    $url = 'https://noahs-creator.com/banner-json';

    // Realizar la solicitud HTTP para obtener el JSON.
    try {
      $client = \Drupal::httpClient();
      $response = $client->request('GET', $url);
      $data = json_decode($response->getBody()->getContents(), TRUE);
    }
    catch (\Exception $e) {
      DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => Error::logException(\Drupal::logger('custom_module'), $e), fn() => watchdog_exception('custom_module', $e));
      return [
        '#type' => 'markup',
        '#markup' => t('Unable to fetch banners at this time.'),
      ];
    }

    // Array para almacenar los elementos renderizados.
    $rendered_banners = [];

    foreach ($data as $banner) {

      $rendered_banners[] = [
        '#type' => 'markup',
        '#markup' => '<a href="' . $banner['field_link'] . '" target="_blank"><img src="' . $banner['field_image'] . '"/></a>',
        '#wrapper_attributes' => [
          'class' => ['swiper-slide'],
        ],
      ];
    }

    return [
      '#theme' => 'item_list',
      '#items' => $rendered_banners,
      '#attributes' => ['class' => 'swiper-wrapper'],
      '#wrapper_attributes' => [
        'class' => ['noahs-banner-list swiper-container'],
        'id' => 'noahs_banners_slide',
      ],
      '#attached' => [
        'library' => [
          'noahs_page_builder/noahs_page_builder.banners',
        ],
      ],
    ];
  }

}
