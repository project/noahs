<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\noahs_page_builder\ControlsManager;
use Drupal\Core\File\FileSystemInterface;
use Drupal\noahs_page_builder_pro\Controller\NoahsBuildThemeProController;
use Drupal\noahs_page_builder\Service\ControlServices;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for domain finder routes.
 */
class NoahsSaveStylesController extends ControllerBase {


  /**
   * Load service.
   *
   * @var \Drupal\noahs_page_builder\Service\ControlServices
   */
  protected $controlService;

  /**
   * NoahsSaveStylesController constructor.
   *
   * @param \Drupal\noahs_page_builder\Service\ControlServices $control_service
   *   The control service used for managing styles.
   */
  public function __construct(ControlServices $control_service) {
    $this->controlService = $control_service;
  }

  /**
   * Creates an instance of the controller.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The instances.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('noahs_page_builder.control_service')
    );
  }



}
