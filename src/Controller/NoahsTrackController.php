<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Track user.
 */
class NoahsTrackController extends ControllerBase {

  /**
   * RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function trackUser($eid) {
    $current_user = \Drupal::currentUser();

    $connection = Database::getConnection();
    $query = $connection->select('noahs_user_track', 'n')
      ->fields('n', ['uid'])
      ->condition('eid', $eid)
      ->execute()
      ->fetchAll();

    if (!empty($query)) {
      if (!in_array($current_user->id(), array_column($query, 'uid'))) {
        \Drupal::messenger()->addWarning('Otro usuario está editando este contenido.');
      }
    }
    else {
      $connection->insert('noahs_user_track')
        ->fields([
          'eid' => $eid,
          'uid' => $current_user->id(),
          'timestamp' => time(),
        ])
        ->execute();
    }

    return [
      '#markup' => "Tracking user with ID {$current_user->id()} on entity {$eid}.",
    ];
  }

}
