<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\editor\Entity\Editor;
use Drupal\noahs_page_builder\ControlsManager;
use Drupal\noahs_page_builder\ModalForm;
use Drupal\noahs_page_builder\Service\WidgetServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\editor\Plugin\EditorManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;



/**
 * Provides route responses for Noah's functions.
 */
class NoahsFunctionsController extends ControllerBase {

  /**
   * The widget services.
   *
   * @var \Drupal\noahs_page_builder\Service\WidgetServices
   */
  protected $widgetService;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Plugin Manager.
   *
   * @var \Drupal\editor\Plugin\EditorManager
   */
  protected $editorPluginManager;

  /**
   * The extension list module service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionList;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The configuration object for noahs_page_builder.settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * NoahsController constructor.
   *
   * @param \Drupal\noahs_page_builder\Service\WidgetServices $widget_service
   *   The widget services.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\editor\Plugin\EditorManager $editor_plugin_manager
   *   The editor plugin manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list
   *   The extension list service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    WidgetServices $widget_service,
    LanguageManagerInterface $language_manager,
    RouteMatchInterface $route_match,
    Connection $database,
    EditorManager $editor_plugin_manager,
    ModuleExtensionList $extension_list,
    AccountInterface $current_user,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->widgetService = $widget_service;
    $this->languageManager = $language_manager;
    $this->routeMatch = $route_match;
    $this->database = $database;
    $this->editorPluginManager = $editor_plugin_manager;
    $this->extensionList = $extension_list;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('noahs_page_builder.settings');
  }

  /**
   * Creates an instance of the controller.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('noahs_page_builder.widget_service'),
      $container->get('language_manager'),
      $container->get('current_route_match'),
      $container->get('database'),
      $container->get('plugin.manager.editor'),
      $container->get('extension.list.module'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get Media Image.
   */
  public function getMediaImage($fid, $image_style) {

    $background_image = NULL;

    if (!empty($fid)) {
      $media = Media::load($fid);
      if ($media && $media->bundle() === 'image') {
        $media_field_name = 'field_media_image';
        if ($media->hasField($media_field_name) && !$media->get($media_field_name)->isEmpty()) {
          $file = $media->get($media_field_name)->entity;
          if (!empty($file) && $file != NULL && !is_null($file->getFileUri())) {
            $file_uri = $file->getFileUri();
            if ($image_style != 'original') {
              $background_image = ImageStyle::load($image_style)->buildUrl($file_uri);
            }
            else {
              $background_image = \Drupal::service('file_url_generator')->generateString($file_uri);
            }
            $css .= 'background-image:' . 'url(' . $background_image . ');';
          }
        }
      }
    } 
    return new JsonResponse($background_image);
  }

  /**
   * Get Media Image Token.
   */
  public function getMediaImageToken($token) {

    $rendered_token = NULL;

    $tokenService = \Drupal::token();

    if (!empty($token)) {
      $token_value = $token;
    }

    if (!empty($token)) {

      $route_match = \Drupal::routeMatch();
      if (!empty($route_match->getParameter('entity')) && !empty($route_match->getParameter('nid'))) {
        $node = \Drupal::entityTypeManager()->getStorage($route_match->getParameter('entity'))->load($route_match->getParameter('nid'));
        $rendered_token = '';
        if (!empty($node)) {
          $rendered_token = $tokenService->replace($token_value, [
            $route_match->getParameter('entity') => $node,
          ]);
        }
        else {
          $rendered_token = $tokenService->replace($token_value);
        }
      }
      elseif (!empty($route_match->getParameter('node'))) {
        $node = $route_match->getParameter('node');
        $rendered_token = '';
        if (!empty($node)) {
          $rendered_token = $tokenService->replace($token_value, [
            $node->getEntityTypeId() => $node,
          ]);
        }
        else {
          $rendered_token = $tokenService->replace($token_value);
        }
      }
    }
    return new JsonResponse($rendered_token);
  }

}
