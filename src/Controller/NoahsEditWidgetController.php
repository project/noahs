<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\noahs_page_builder\ModalForm;

/**
 * Controller routines for domain finder routes.
 */
class NoahsEditWidgetController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function edit($nid, $widget, $widget_id) {

    $noahs_page_builder_config = \Drupal::config('noahs_page_builder.settings');
    $widgetService = \Drupal::service('noahs_page_builder.widget_service');

    $pallete_color = [];

    $pallete_color[] = !empty($noahs_page_builder_config->get('principal_color')) ? $noahs_page_builder_config->get('principal_color') : '#2389ab';
    $pallete_color[] = !empty($noahs_page_builder_config->get('secondary_color')) ? $noahs_page_builder_config->get('principal_color') : '#4a4a4a';
    $pallete_color[] = !empty($noahs_page_builder_config->get('heading_color')) ? $noahs_page_builder_config->get('heading_color') : '#4a4a4a';
    $pallete_color[] = !empty($noahs_page_builder_config->get('text_color')) ? $noahs_page_builder_config->get('text_color') : '#000000';

    $page['#attached']['drupalSettings']['noahs_page_builder']['pallete_color'] = $pallete_color;

    $fields = $widgetService->getWidgetFields($widget);

    $page['#attached']['drupalSettings']['noahs_page_builder']['field_settings'] = $settings;

    $jsonData = json_encode($settings);
    $escapedJsonData = htmlspecialchars($jsonData, ENT_QUOTES, 'UTF-8');

    $form = ModalForm::renderForm($fields, $settings);

    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $output = '<form class="form-widget" id="noahs_page_builder_edit_widget_form">';
    $output .= '<input type="hidden" name="wid" value="' . $widget_id . '"/>';
    $output .= '<input type="hidden" name="did" value="' . $nid . '"/>';
    $output .= '<input type="hidden" name="uid" value="' . \Drupal::currentUser()->id() . '"/>';
    $output .= '<input type="hidden" name="type" value="' . $widget . '"/>';
    $output .= '<input type="hidden" name="langcode" value="' . $langcode . '"/>';
    $output .= '<div class="form-data-settings" data-settings="' . $escapedJsonData . '"></div>';

    foreach ($form as $item) {

      $output .= $item;
    }
    $output .= '<div class="btn-group-margin">
    <button type="submit" class="btn btn-success btn-labeled save-widget"><span class="btn-label"><i class="fa-regular fa-floppy-disk"></i></span>Save</button>
    <button class="btn btn-danger btn-labeled noahs_page_builder-close-modal"><span class="btn-label"><i class="fa-solid fa-xmark"></i></span>Close</button>
    </div>';
    $output .= '</form>';

    $page['#attached']['library'][] = 'noahs_page_builder/noahs_page_builder.assets.admin';

    $page['noahs_page_builder-admin-edit-widget'] = [
      '#theme' => 'noahs-admin-edit-widget',
      '#content' => $output,
    ];

    return $page;

  }

}
