<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class NoahsModalTokensController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function outputTree() {

    $token_entity_mapping = \Drupal::service('token.entity_mapper');

    // Obtener una lista de tokens disponibles.
    $all_tokens = $token_entity_mapping->getEntityTypeMappings();
    $tree = \Drupal::service('token.tree_builder')->buildAllRenderable(array_keys($all_tokens));
    $build = $tree;
    $build['#cache']['contexts'][] = 'url.query_args:options';
    $build['#title'] = $this->t('Available tokens');
    $build['#prefix'] = '<div>';
    $build['#suffix'] = '</div>';

    return $build;
  }

}
