<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

/**
 * {@inheritdoc}
 */
class NoahsUrlAutocompleteController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function autocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');
    if (!$input) {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);
    $query = \Drupal::entityQuery('node')
      ->condition('title', $input, 'CONTAINS')
      ->groupBy('nid')
      ->sort('created', 'DESC')
      ->range(0, 10)
      ->accessCheck(FALSE);
    $ids = $query->execute();
    $nodes = $ids ? Node::loadMultiple($ids) : [];
    foreach ($nodes as $node) {
      $results[] = [
        'id' => $node->id(),
        'text' => $node->getTitle() . ' (' . $node->id() . ')',
      ];
    }

    return new JsonResponse($results);
  }

}
