<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\media\Entity\Media;
use Drupal\views\Views;

/**
 * {@inheritdoc}
 */
class NoahsModalMediaController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function mediaModal(Request $request) {
    $element_id = $request->query->get('element_id');
    $type = $request->query->get('type');
    $source = $request->query->get('source');
    $wid = $request->query->get('wid');

    // Preparar el HTML para la lista de miniaturas.
    $view = Views::getView('noahs_media_modal');
    $display_id = Views::getView('block_1');
    if ($source === 'video') {
      $display_id = Views::getView('block_3');
    }

    if ($view) {
      $args = NULL;
      $args = [$args];

      $view->setDisplay($display_id);
      $view->setArguments($args);
      $view->initHandlers();
      $view->preExecute();
      $view->execute();
      $view_render = $view->buildRenderable($display_id);
      return [
        '#theme' => 'noahs_media_modal_page',
        '#view_render' => $view_render,
        '#source' => $source,
        '#element_id' => $element_id,
        '#wid' => $wid,
        '#type' => $type,
      ];
    }
    else {
      return [
        '#markup' => '<p>No se pudo cargar el contenido.</p>',
      ];

    }
  }

  /**
   * {@inheritdoc}
   */
  public function uploadMediaModal() {
    $files = $_FILES['files'] ?? NULL;
    $current_date = \Drupal::time()->getCurrentTime();
    $current_month = date('Y-m', $current_date);

    $directory = 'public://' . $current_month;
    $file_system = \Drupal::service('file_system');
    $file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::EXISTS_RENAME);

    $uploaded_files = [];

    foreach ($files['name'] as $key => $filename) {
      $filedata = file_get_contents($files['tmp_name'][$key]);
      $directory_url = $directory . '/' . $filename;

      // Guardar el archivo en el sistema de archivos.
      $uploaded_file = $file_system->saveData($filedata, $directory_url, FileSystemInterface::EXISTS_RENAME);

      // Crear la entidad de archivo.
      $file = File::create([
        'uri' => $uploaded_file,
      ]);
      $file->setPermanent();
      $file->save();

      // Crear la entidad media.
      $media = Media::create([
      // Ajusta el tipo de media según sea necesario.
        'bundle' => 'image',
        'name' => $filename,
        'field_media_image' => [
          'target_id' => $file->id(),
        ],
      ]);
      $media->save();

      // Generar las URLs para el archivo y la imagen.
      $image_style_url = '';
      $media_url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());

      // Si no es un SVG, construir la URL de estilo de imagen.
      if ($file->getMimeType() != "image/svg+xml") {
        $image_style_url = ImageStyle::load('thumbnail')->buildUrl($file->getFileUri());
      }

      // Cambié esto para obtener el nombre del archivo correctamente.
      $file_name = $file->getFilename();
      $uploaded_files[] = [
        'file_id' => $media->id(),
        'file_url' => $image_style_url,
        'file_name' => $file_name,
        'file_type' => $file->getMimeType(),
        'file_fullurl' => $media_url,
      ];
    }

    if (empty($uploaded_files)) {
      return new JsonResponse(['message' => 'No se ha enviado ningún archivo.'], 400);
    }

    return new JsonResponse(['message' => 'Archivos subidos correctamente.', 'files' => $uploaded_files]);
  }

  /**
   * {@inheritdoc}
   */
  public function uploadCkMediaModal(Request $request) {

    $file_system = \Drupal::service('file_system');
    $file_url_generator = \Drupal::service('file_url_generator');
    $file = $request->files->get('upload');

    if ($file) {

      $directory = 'public://ckeditor_uploads/';
      $file_system->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
      $filename = $file_system->copy($file->getRealPath(), $directory . $file->getClientOriginalName(), FileSystemInterface::EXISTS_RENAME);

      $file_entity = File::create([
        'uri' => $filename,
        'status' => 1,
      ]);
      $file_entity->save();

      $file_url = $file_url_generator->generateAbsoluteString($file_entity->getFileUri());

      return new JsonResponse(['url' => $file_url]);
    }

    return new JsonResponse(['error' => 'Error uploading image'], 400);
  }

}
