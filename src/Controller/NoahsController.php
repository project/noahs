<?php

namespace Drupal\noahs_page_builder\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\editor\Entity\Editor;
use Drupal\noahs_page_builder\ControlsManager;
use Drupal\noahs_page_builder\ModalForm;
use Drupal\noahs_page_builder\Controller\NoahsSaveStylesController;
use Drupal\noahs_page_builder\Service\WidgetServices;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\editor\Plugin\EditorManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides route responses for Noah's Builder.
 */
class NoahsController extends ControllerBase {

  /**
   * The widget services.
   *
   * @var \Drupal\noahs_page_builder\Service\WidgetServices
   */
  protected $widgetService;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Plugin Manager.
   *
   * @var \Drupal\editor\Plugin\EditorManager
   */
  protected $editorPluginManager;

  /**
   * The extension list module service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionList;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The configuration object for noahs_page_builder.settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * NoahsController constructor.
   *
   * @param \Drupal\noahs_page_builder\Service\WidgetServices $widget_service
   *   The widget services.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\editor\Plugin\EditorManager $editor_plugin_manager
   *   The editor plugin manager.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list
   *   The extension list service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    WidgetServices $widget_service,
    LanguageManagerInterface $language_manager,
    RouteMatchInterface $route_match,
    Connection $database,
    EditorManager $editor_plugin_manager,
    ModuleExtensionList $extension_list,
    AccountInterface $current_user,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->widgetService = $widget_service;
    $this->languageManager = $language_manager;
    $this->routeMatch = $route_match;
    $this->database = $database;
    $this->editorPluginManager = $editor_plugin_manager;
    $this->extensionList = $extension_list;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('noahs_page_builder.settings');
  }

  /**
   * Creates an instance of the controller.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('noahs_page_builder.widget_service'),
      $container->get('language_manager'),
      $container->get('current_route_match'),
      $container->get('database'),
      $container->get('plugin.manager.editor'),
      $container->get('extension.list.module'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Editor builder.
   */
  public function editor($entity_type, $entity) {

    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $default_languagecode = $this->languageManager->getDefaultLanguage()->getId();
    $widgets = $this->widgetService->getWidgetsList();
    $all_widgets = $this->widgetService->getAllWidgetsList();
    $globlal_widgets = [];
    $defaults_widgets = [];
    $eid = $entity->id();
    $noahs_id = $eid;
    $save_page_url = Url::fromRoute('noahs_page_builder.save_page', [], ['absolute' => TRUE])->toString();

    if ($langcode === $default_languagecode) {
      $iframe_url = "/noahs_edit/preview/{$entity_type}/{$eid}";
    }
    else {
      $iframe_url = "/{$langcode}/noahs_edit/preview/{$entity_type}/{$eid}";
    }

    if (is_module_installed('noahs_page_builder_pro')) {
      $globlal_widgets = noahs_page_builder_pro_load_widgets();
      $defaults_widgets = noahs_page_builder_pro_load_defaults_widgets();
    }

    $fields = [];
    foreach($all_widgets as $k => $el){
      $fields[$k] = $this->widgetService->getWidgetFields($k);
    }

    $page['#attached']['drupalSettings']['noahs_items_fields'] = $fields;
    $page['#attached']['drupalSettings']['savePage'] = $save_page_url;
    $page['#attached']['drupalSettings']['entity_id'] = $noahs_id;
    $page['#attached']['drupalSettings']['noahs_id'] = $noahs_id;
    $page['#attached']['drupalSettings']['entity_type'] = $entity_type;
    $page['#attached']['drupalSettings']['uid'] = $this->currentUser->id();
    $page['#attached']['drupalSettings']['all_widgets'] = $all_widgets;
    $page['#attached']['drupalSettings']['langcode'] = $langcode;
    $page['#attached']['library'][] = 'noahs_page_builder/noahs_page_builder.assets.preview';
    $page['#attached']['library'][] = 'noahs_page_builder/noahs_page_builder.media_library_modal';
    $page['#attached']['library'][] = 'noahs_page_builder/noahs_page_builder.media_library_form_element';

    // CKEditor Drupal settings.
    $noahs_page_builder_config = $this->config('noahs_page_builder.settings');

    if (!empty($noahs_page_builder_config->get('use_editor'))) {
      $editor = Editor::load($noahs_page_builder_config->get('use_editor'));
      $plugin = $this->editorPluginManager->createInstance($editor->getEditor());
      $settings = $plugin->getJSSettings($editor);

      $page['#attached']['library'] = array_merge($page['#attached']['library'], $plugin->getLibraries($editor));
      $page['#attached']['drupalSettings']['noahs_page_builder']['ckeditor_settings'] = $settings;
    }

    $page['noahs-admin-form'] = [
      '#theme' => 'noahs-admin-form',
      '#url' => $entity->toUrl()->toString(),
      '#content' => '',
      '#noahs_id' => $eid,
      '#entity_type' => $entity_type,
      '#langcode' => $langcode,
      '#widgets' => $widgets,
      '#globlal_widgets' => $globlal_widgets,
      '#defaults_widgets' => $defaults_widgets,
      '#iframe_url' => $iframe_url,
      '#noahs_pro' => is_module_installed('noahs_page_builder_pro'),
    ];

    return $page;
  }

  /**
   * Preview (iframe) builder.
   */
  public function preview($entity_type, $entity) {

    $eid = $entity->id();
    $classes = [];
    $data_attributes = [];
    $noahs_id = $eid;
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $widgets = $this->widgetService->getWidgetsList();
    $data = noahs_page_builder_load($langcode, $noahs_id, $entity_type) ?? FALSE;
    $page_settings = !empty($data->page_settings) ? json_decode($data->page_settings, TRUE) : [];
    $classes[] = $this->getClasses($data, 'class');
    $data_attributes[] = $this->getClasses($data, 'attributes');
    $sections = noahs_page_builder_get_sections($data->settings);
    $getPageUrl = Url::fromRoute('noahs_page_builder.save_page', [], ['absolute' => TRUE])->toString();
    $module_url = '/' . $this->extensionList->getPath('noahs_page_builder');
    $html = noahs_page_builder_html_generated($sections);

    $page['#attached']['drupalSettings']['savePage'] = $getPageUrl;
    $page['#attached']['drupalSettings']['noahs_page_builder']['base_path'] = base_path();
    $page['#attached']['drupalSettings']['noahs_page_builder']['classes'] = $classes;
    $page['#attached']['drupalSettings']['noahs_page_builder']['attributes'] = $data_attributes;
    $page['#attached']['drupalSettings']['module_url'] = $module_url;
    $page['#attached']['drupalSettings']['entity_id'] = $noahs_id;
    $page['#attached']['drupalSettings']['entity_type'] = $entity_type;
    $page['#attached']['drupalSettings']['noahs_id'] = $noahs_id;
    $page['#attached']['drupalSettings']['uid'] = $this->currentUser->id();
    $page['#attached']['drupalSettings']['langcode'] = $langcode;
    
    $page['#attached']['library'][] = 'noahs_page_builder/noahs_page_builder.assets.admin';
    $page['#attached']['library'][] = 'noahs_page_builder/noahs_page_builder.assets.frontend';

    $page['noahs-admin-preview'] = [
      '#content' => '',
      '#theme' => 'noahs-admin-preview',
      '#page_settings' => $page_settings,
      '#entity_type' => $entity_type,
      '#langcode' => $langcode,
      '#widgets' => $widgets,
      '#noahs_pro' => is_module_installed('noahs_page_builder_pro'),
      '#generated_html' => $html,
    ];

    return $page;
  }

  /**
   * Get Widgets ids.
   */
  private function getAllIds($array, &$ids) {

    foreach ($array as $key => $value) {
      if ($key === 'id') {
        $entry = ['id' => $value];
        if (isset($array['type'])) {
          $entry['type'] = $array['type'];
        }
        $ids[] = $entry;
      }
      elseif (is_array($value)) {
        $this->getAllIds($value, $ids);
      }
    }
    return $ids;
  }

  /**
   * Clone Page Function.
   */
  public function clonePage($noahs_id, $new_noahs_id, $original_langcode, $new_langcode, $entity_type) {

    $result = '';
    $original = $this->database->select('noahs_page_builder_page', 'd')
      ->fields('d')
      ->condition('noahs_id', $noahs_id)
      ->condition('entity_type', $entity_type)
      ->condition('langcode', $original_langcode)
      ->execute()
      ->fetchAssoc();

    if ($original != NULL) {
      if ($new_noahs_id != NULL) {
        $this->database->insert("noahs_page_builder_page")
          ->fields([
            'noahs_id' => $new_noahs_id,
            'uid' => $original['uid'],
            'entity_id' => $new_noahs_id,
            'langcode' => $new_langcode,
            'settings' => $original['settings'],
            'page_settings' => $original['page_settings'],
            'entity_type' => $entity_type,
          ])
          ->execute();
        $result = 'Página clonada correctamente';
      }
    }
    return $result;
  }

  /**
   * Save Page Function.
   */
  public function savePage(Request $request) {
   
    $uid = $request->request->get('uid');
    $noahs_id = $request->request->get('noahs_id');
    $entity_id = $request->request->get('entity_id');
    $entity_type = $request->request->get('entity_type');
    $settings = $request->request->get('settings');
    $page_settings = $request->request->get('page_settings');
    $css = $request->request->get('css') ?? '';

    if(!empty($request->request->get('language'))){
      $langcode = ($request->request->get('language') === 'no_language') ? 'no_language' : $request->request->get('language');
    }else{
      $langcode = $this->languageManager->getCurrentLanguage()->getId();
    }

    $builder = $this->database->select('noahs_page_builder_page', 'd')
      ->fields('d', ['noahs_id'])
      ->condition('noahs_id', $noahs_id)
      ->condition('entity_type', $entity_type)
      ->condition('langcode', $langcode)
      ->execute()
      ->fetchAssoc();

    if ($builder != NULL) {
      $this->database->update("noahs_page_builder_page")
        ->fields([
          'settings' => $settings,
          'page_settings' => $page_settings,
          'entity_type' => $entity_type,
          'modified_date' => time(),
        ])
        ->condition('noahs_id', $noahs_id)
        ->condition('langcode', $langcode)
        ->execute();

      if (is_module_installed('noahs_page_builder_pro')) {
        $revisions = $this->database->select('noahs_page_builder_revisions', 'd')
          ->fields('d', ['page_id'])
          ->condition('noahs_id', $noahs_id)
          ->condition('entity_type', $entity_type)
          ->condition('langcode', $langcode)
          ->orderBy('modified_date', 'ASC')
          ->execute()
          ->fetchAllAssoc('page_id');

        if (count($revisions) >= 8) {
          $revisions_to_delete = array_slice($revisions, 0, count($revisions) - 7);
          $ids_to_delete = array_column($revisions_to_delete, 'page_id');

          if (!empty($ids_to_delete)) {
            $this->database->delete('noahs_page_builder_revisions')
              ->condition('page_id', $ids_to_delete, 'IN')
              ->execute();
          }
        }

        $this->database->insert('noahs_page_builder_revisions')
          ->fields([
            'noahs_id' => $noahs_id,
            'uid' => $uid,
            'entity_id' => $entity_id,
            'entity_type' => $entity_type,
            'langcode' => $langcode,
            'settings' => $settings,
            'page_settings' => $page_settings,
            'modified_date' => time(),
          ])
          ->execute();
      }

    }
    else {

      $builder = $this->database->insert("noahs_page_builder_page")
        ->fields([
          'noahs_id' => $noahs_id,
          'uid' => $uid,
          'entity_id' => $entity_id,
          'entity_type' => $entity_type,
          'langcode' => $langcode,
          'settings' => $settings,
          'page_settings' => $page_settings,
          'modified_date' => time(),
        ])
        ->execute();

    }
    $css = $this->saveCss($entity_type, $entity_id, $langcode, $css);
    $widgetsDefultCss = $this->saveDefaultWidgetCss();
    return new JsonResponse(['noahs_id' => $noahs_id]);
  }

  /**
   * Save CSS.
   */

   public function saveCss($entity_type, $entity_id, $langcode, $css) {

    $widgetService = \Drupal::service('noahs_page_builder.widget_service');
    $moduleHandler = \Drupal::service('module_handler');


    $directory = 'public://noahs/' . $entity_type;
    $file_system = \Drupal::service('file_system');
    $file_system->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::EXISTS_REPLACE);

    // Nombre original del archivo.
    $prefix_filename = 'noahs_';
    if ($entity_type === 'noahs_settings') {
      $prefix_filename = '';

      if($entity_id === 'noahs_general_settings'){
        $entity_id = 'general_settings';
      }else{
        $entity_id = 'settings';
      }
    }

    if($langcode && $langcode != 'no_language'){
      $filename = $prefix_filename . $entity_id . '_' . $langcode . '.css';
    }else{
      $filename = $prefix_filename . $entity_id . '.css';
    }
   
    \Drupal::service('file_system')->saveData($css, $directory . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);

    return new JsonResponse(['message' => 'CSS saved!', 'styles' => $css]);

  }

   public function saveDefaultWidgetCss() {

    $css = '';
    $builder = $this->database->select('noahs_page_builder_pro_default_widgets', 'd')
    ->fields('d', ['settings'])
    ->execute()
    ->fetchAll();

    if($builder){
      foreach($builder as $result){
        $css .= $result->settings;
      }

      $directory = 'public://noahs/default_widgets';
      $file_system = \Drupal::service('file_system');
      $file_system->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::EXISTS_REPLACE);
  
      // Nombre original del archivo.
      $filename = 'default_widgets_styles.css';
      \Drupal::service('file_system')->saveData($css, $directory . '/' . $filename, FileSystemInterface::EXISTS_REPLACE);
    }
    

  }
   
  /**
   * Get widget form.
   */
  public function getWidgetForm(Request $request) {

    $data = json_decode($request->getContent(), TRUE);
    $fields = $this->widgetService->getWidgetFields($data['widget_id']);
    $form = ModalForm::renderForm($fields, $data);
    return new JsonResponse(['html' => '<form>' . $form[0].'</form>']);

  }

  /**
   * Get rendered widget.
   */
  public function renderWidget(Request $request) {
    $data = json_decode($request->getContent(), TRUE);
    // Crear settings base

    $settings = [
        "type" => $data['widget_id'],
        "wid" => uniqid(),
        "noahs_id" => $data['nid'],
        "settings" => [
            "element" => $data['form_data']['element'],
        ]
    ];

    $column_size = !empty($data['extra_data']['width']) ? $data['extra_data']['width'] : FALSE;

    if ($column_size === 'two_columns') {
        return new JsonResponse(['html' => $this->getDinamicColumns('2', $data)]);
    } elseif ($column_size === 'three_columns') {
        return new JsonResponse(['html' => $this->getDinamicColumns('3', $data)]);
    } elseif ($column_size === 'four_columns') {
        return new JsonResponse(['html' => $this->getDinamicColumns('4', $data)]);
    } else {
        // Modificar settings si es "noahs_column"
        if ($data['widget_id'] === 'noahs_column') {
            $settings['settings']['element'] = [
                "css" => [
                  "desktop" => [
                      "default" => ["column_width" => $column_size]
                  ],
                  "mobile" => [
                      "default" => ["column_width" => "100%"]
                  ]
                ]
            ];
        }

        // Convertir todo el array en objeto recursivamente
        $settings = json_decode(json_encode($settings), FALSE);

        $widget = noahs_page_builder_render_element($settings, NULL);
        return new JsonResponse(['html' => $widget, 'obj' => $settings]);
    }
}


  /**
   * Get Dinamic Columns.
   */
  public function getDinamicColumns($num_columns, $data) {
    $rendered_columns = [];

    for ($i = 0; $i < $num_columns; $i++) {
        if ($data['widget_id'] === 'noahs_column') {
            $width = (100 / $num_columns) . '%';

            $settings = [
              "type" => $data['widget_id'],
              "wid" => uniqid(),
              "noahs_id" => $data['nid'],
              "settings" => [
                  "element" => [
                    "css" => [
                      'desktop' => ['default' => ['column_width' => $width]],
                      'tablet'  => ['default' => ['column_width' => '50%']],
                      'mobile'  => ['default' => ['column_width' => '100%']]
                    ]
                  ]
              ]
            ];
    
        }

        $settings = json_decode(json_encode($settings), FALSE);
        $widget = noahs_page_builder_render_element($settings, NULL);
        $rendered_columns[] = $widget;
    }

    return implode('', $rendered_columns);
}

  
  /**
   * Regenerate widget.
   */
  public function regenerateWidget(Request $request) {

    $data = json_decode($request->getContent());
    $obj = new \stdClass();
    $obj->type = $data->settings->type;
    $obj->wid = $data->widget_id;
    $obj->noahs_id = $data->settings->noahs_id;
    $obj->settings = $data->settings;

    $widget = noahs_page_builder_render_element($obj, NULL);

    return new JsonResponse(['html' => $widget]);
  }

  /**
   * Get default data widget.
   */
  public function renderDefaultTemplateWidget($type) {

    $widget = noahs_page_builder_render_default_template($type);

    return new JsonResponse(['html' => $widget]);
  }

  /**
   * Get Css Classes.
   */
  public function getClasses($data, $type) {

    $page_settings = !empty($data->settings) ? json_decode($data->settings, TRUE) : [];
    $classes = [];
    if (!empty($data->page_settings)) {
      $theme_settings = json_decode($data->page_settings, TRUE);
      $classes[] = $this->getPageClasses($theme_settings['page']);
    }

    foreach ($page_settings as $item) {
      $classes = array_merge($classes, $this->extractClasses($item, $type));
    }

    return $classes;
  }

  /**
   * Manual get page classes.
   */
  public function getPageClasses($data) {
    $classes = [];

    if (!empty($data['columns_login_checkout'])) {
      $classes['.checkout-pane-login'] = $data['columns_login_checkout'];
    }
    if (!empty($data['columns_information_checkout'])) {
      $classes['.noahs-checkout-step-order_information .layout-checkout-form'] = $data['columns_information_checkout'];
    }

    return $classes;
  }

  /**
   * Get Extra classes.
   */
  private function extractClasses($item, $type) {
    $classes = [];
    $obj_class = new ControlsManager();

    if (!empty($item['settings'])) {
      $widget_settings = $item['settings'];
      $fields = $this->widgetService->getWidgetFields($item['type']);

      if ($type === 'class' && !empty($widget_settings['element']['class'])) {
        $data_classes = $obj_class->getClasses($fields, $widget_settings['element']['class'], $item['wid']);
        $classes[] = $data_classes;
      }
      if ($type === 'attributes' && !empty($widget_settings['element']['attribute'])) {
        $data_attributes = $obj_class->getAttributes($fields, $widget_settings['element']['attribute'], $item['wid']);
        if ($data_attributes) {
          $classes[] = $data_attributes;
        }
      }
      if (!empty($item['columns'])) {
        foreach ($item['columns'] as $element) {
          $classes = array_merge($classes, $this->extractClasses($element, $type));
        }
      }

      if (!empty($item['elements'])) {
        foreach ($item['elements'] as $element) {
          $classes = array_merge($classes, $this->extractClasses($element, $type));

        }
      }

    }

    return $classes;
  }

}
