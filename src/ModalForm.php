<?php

namespace Drupal\noahs_page_builder;

/**
 * {@inheritdoc}
 */
class ModalForm {

  /**
   * Public function contentTemplate().
   */
  public static function renderForm($fields, $values, $delta = NULL, $parent = NULL) {
    $controlService = \Drupal::service('noahs_page_builder.control_service');
    $form = [];

    $groupFields = $controlService->groupFields($fields);

    $tabs_class = new ControlsManager();

    $data_controls = $tabs_class->renderTabs($groupFields, $values);

    $form[] = $data_controls['form'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function renderSubFields($fields, $values = NULL, $delta = NULL, $parent = NULL) {

    // Group by tabs.
    $tabs = [];

    foreach ($fields as $key => $value) {

      if (isset($value['type']) && $value['type'] === 'tab') {

        $currentTab = $key;
        $tabs[$currentTab] = $value;
        $tabs[$currentTab]['items'] = [];
        // Agregar el título de la pestaña.
        $tabs[$currentTab]['title'] = $value['title'] ?? $currentTab;
      }
      else {
        if (isset($value['tab'])) {
          $tabName = $value['tab'];
          $group = $value['group'] ?? NULL;

          // Verificar si hay grupos o no.
          if ($group !== NULL) {
            $tabs[$tabName]['items'][$group]['title'] = $value['title'] ?? $group;
            $tabs[$tabName]['items'][$group][$key] = $value;
          }
          else {
            $tabs[$tabName]['items'][$key] = $value;
          }
        }
      }
    }

    $tabs_class = new ControlsManager();

    $data_controls = $tabs_class->renderTabs($tabs, $values, 'multiple', $delta, $parent);

    return $data_controls['form'];
  }

}
