<?php

namespace Drupal\noahs_page_builder\Plugin\Control;

use Drupal\Component\Plugin\PluginBase;

/**
 * Clase base para los plugins de Control.
 */
abstract class ControlBase extends PluginBase implements ControlInterface {

  /**
   * {@inheritdoc}
   */
  public function getImageStyles() {
    $widgetServices = \Drupal::service('noahs_page_builder.widget_service');
    return $widgetServices->getImageStyle();
  }

}
