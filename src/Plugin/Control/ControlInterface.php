<?php

namespace Drupal\noahs_page_builder\Plugin\Control;

/**
 * Controls Interface.
 */
interface ControlInterface {

  /**
   * Run control.
   *
   * @return array
   *   Render.
   */
  public function getype();

  /**
   * {@inheritdoc}
   */
  public function contentTemplate(array $params = []);

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings();

}
