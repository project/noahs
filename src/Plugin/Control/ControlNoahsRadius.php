<?php

namespace Drupal\noahs_page_builder\Plugin\Control;

/**
 * @ControlPlugin(
 *   id = "noahs_radius",
 *   label = @Translation("Radius")
 * )
 */
class ControlNoahsRadius extends ControlBase {

  /**
   * {@inheritdoc}
   */
  public function getype() {
    return 'noahs_radius';
  }

  /**
   * {@inheritdoc}
   */
  public function contentTemplate(array $params = []) {
    $data = $params['data'] ?? NULL;
    $name = $params['name'] ?? NULL;
    $value = $params['value'] ?? NULL;
    $output = '';

    $output .= '<ul class="field-element-list-horizontal mb-3">';
    $output .= '<li>';
    $output .= '<input type="text" name="' . htmlspecialchars($name, ENT_QUOTES, 'UTF-8') . '[border_top_left_radius]" value="' . (!empty($value['border_top_left_radius']) ? htmlspecialchars($value['border_top_left_radius'], ENT_QUOTES, 'UTF-8') : '') . '" class="form-control" field-settings>';
    $output .= '<label for="noahs_page_builder_border-top-left-radius">Top</label>';
    $output .= '</li>';
    $output .= '<li>';
    $output .= '<input type="text" name="' . htmlspecialchars($name, ENT_QUOTES, 'UTF-8') . '[border_top_right_radius]" value="' . (!empty($value['border_top_right_radius']) ? htmlspecialchars($value['border_top_right_radius'], ENT_QUOTES, 'UTF-8') : '') . '" class="form-control" field-settings>';
    $output .= '<label for="noahs_page_builder_border_top_right_radius">Right</label>';
    $output .= '</li>';
    $output .= '<li>';
    $output .= '<input type="text" name="' . htmlspecialchars($name, ENT_QUOTES, 'UTF-8') . '[border_bottom_right_radius]" value="' . (!empty($value['border_bottom_right_radius']) ? htmlspecialchars($value['border_bottom_right_radius'], ENT_QUOTES, 'UTF-8') : '') . '" class="form-control" field-settings>';
    $output .= '<label for="noahs_page_builder_shadow_border_bottom_right_radius">Bottom</label>';
    $output .= '</li>';
    $output .= '<li>';
    $output .= '<input type="text" name="' . htmlspecialchars($name, ENT_QUOTES, 'UTF-8') . '[border_bottom_left_radius]" value="' . (!empty($value['border_bottom_left_radius']) ? htmlspecialchars($value['border_bottom_left_radius'], ENT_QUOTES, 'UTF-8') : '') . '" class="form-control" field-settings>';
    $output .= '<label for="noahs_page_builder_shadow_border_bottom_left_radius">Left</label>';
    $output .= '</li>';
    $output .= '</ul>';

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings() {
    return [
      'input_type' => 'noahs_radius',
      'placeholder' => '',
      'title' => '',
    ];
  }

}
