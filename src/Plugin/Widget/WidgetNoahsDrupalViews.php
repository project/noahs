<?php

namespace Drupal\noahs_page_builder\Plugin\Widget;

use Drupal\views\Views;

/**
 * @WidgetPlugin(
 *   id = "noahs_drupal_views",
 *   label = @Translation("View")
 * )
 */
class WidgetNoahsDrupalViews extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function data() {
    return [
      'icon' => '<i class="fa-brands fa-drupal"></i>',
      'title' => 'Drupal Views',
      'description' => 'Description',
      'group' => 'Drupal',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function renderForm() {
    $form = [];
    $options = noahs_page_builder_load_views();

    // Section Content.
    $form['section_content'] = [
      'type' => 'tab',
      'title' => t('Content'),
    ];

    $form['drupal_block'] = [
      'type'    => 'select',
      'select_group_views'    => TRUE,
      'title'   => t('Drupal Block'),
      'tab' => 'section_content',
      'options' => $options,
      'wrapper' => FALSE,
      'attributes' => [
        'class' => 'noahs-regenerate-design',
      ],
    ];

    $form['token'] = [
      'type'    => 'text',
      'title'   => t('Token'),
      'tab' => 'section_content',
      'description' => t('Use static or consult a token, separe with coma (,)'),
    ];
    $form['show_items'] = [
      'type'    => 'number',
      'title'   => t('Show items'),
      'tab' => 'section_content',
      'description' => t('Set the number of items to display'),
    ];
    $form['hide_pagination'] = [
      'type'    => 'checkbox',
      'title'   => t('Hide Pagination'),
      'value' => 'true',
      'default_value' => 'false',
      'tab' => 'section_content',
      'wrapper' => FALSE,
      'attributes' => [
        'class' => 'noahs-regenerate-design',
      ],
    ];
    $form['hide_exposed_filter'] = [
      'type'    => 'checkbox',
      'title'   => t('Hide Exposed Filters'),
      'value' => 'true',
      'default_value' => 'false',
      'tab' => 'section_content',
      'wrapper' => FALSE,
      'attributes' => [
        'class' => 'noahs-regenerate-design',
      ],
    ];
    $form['token_button'] = [
      'type'    => 'html',
      'value'   => '<a class="btn btn-s btn-info noahs_page_builder-modal-tokens mb-4" href="#">Select Token</a>',
      'tab' => 'section_content',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function template($settings) {
    $render_block = '';
    $settings = $settings->element;

    if (!empty($settings->drupal_block)) {

      $views_array = json_decode($settings->drupal_block);
      $view = Views::getView($views_array[0]);

      $display_id = $views_array[1];
      $render_block = '<div>Missing view, block "' . $settings->drupal_block . '"</div>';

      $args = NULL;
      if (!empty($settings->token->text)) {
        $token_service = \Drupal::token();
        $token = $settings->token->text;
        $rendered_token = $token_service->replace($token);
        $args = $rendered_token;

      }

      $args = [$args];

      if ($view) {

        $view->setDisplay($display_id);
        $view->setArguments($args);

        if (!empty($settings->show_items)) {
          $view->setItemsPerPage($settings->show_items);
          $view->setCurrentPage(0);
          $view->setOffset(0);
        }
        if (!empty($settings->hide_exposed_filter)) {
          $view->display_handler->options['exposed_block'] = TRUE;
        }

        $view->initHandlers();
        $view->preExecute();
        $view->execute();

        if (!empty($settings->hide_pagination)) {
          unset($view->pager);
        }
        $view_render = $view->buildRenderable($display_id);
        $render_block = \Drupal::service('renderer')->render($view_render);
        $render_block = !empty($render_block) ? $render_block->__toString() : NULL;

      }

    }

    $output = '';

    $output .= '<div class="widget-content">';
    if (!empty($render_block)) {
      $output .= $render_block;
    }
    else {
      $output .= '<div class="drupal-viewblock-empty">Drupal View</div>';
    }
    $output .= '</div>';

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function renderContent($element, $content = NULL) {
    return $this->wrapper($element, $this->template($element->settings));
  }

}
