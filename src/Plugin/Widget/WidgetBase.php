<?php

namespace Drupal\noahs_page_builder\Plugin\Widget;

use Drupal\noahs_page_builder\Controller\NoahsSaveStylesController;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Url;

/**
 * Clase base para los plugins de Widget.
 */
abstract class WidgetBase extends PluginBase implements WidgetInterface {

  /**
   * {@inheritdoc}
   */
  public function twig() {
    return \Drupal::service('twig');
  }

  /**
   * {@inheritdoc}
   */
  public function getImageStyles() {
    $widgetServices = \Drupal::service('noahs_page_builder.widget_service');
    return $widgetServices->getImageStyle();
  }

  /**
   * {@inheritdoc}
   */
  public function wrapper($data, $content) {

    $widgetService = \Drupal::service('noahs_page_builder.widget_service');
    $config = \Drupal::config('noahs_page_builder.settings');

    $divider_declaration = '';

    $settings = !empty($data->settings->element) ? $data->settings->element : NULL;

    if (!empty($settings->css->desktop->default->bg_divider)) {
      $dividers = $settings->css->desktop->default->bg_divider;

      foreach ($dividers as $k => $divider) {

        if (!empty($divider->divider)) {
          if ($k === 'bottom') {
            $divider_declaration .= '<div class="noahs-divider-svg bottom">';
          }
          if ($k === 'top') {
            $divider_declaration .= '<div class="noahs-divider-svg top">';

          }
          $divider_declaration .= file_get_contents($divider->divider);
          $divider_declaration .= '</div>';
        }
      }

    }

    $element = 'div';
    $class = ['noahs_page_builder-widget', 'widget-' . str_replace('_', '-', $data->type)];
    $subclass = ['widget-wrapper', 'element-transition'];
    $widget_default = 'element';
    $column_size = NULL;
    $obj = new \stdClass();
    $obj->settings = new \stdClass();

    $global_class = !empty($data->settings->global) ? 'widget-global' : '';
    $tabs = "<ul class='noahs_page_builder-widget-action-tabs tab-{$data->type} $global_class'>";
    $id = !empty($data->wid) ? $data->wid : uniqid();
    $uid = \Drupal::currentUser()->id();

    $fields = $widgetService->getWidgetFields($data->type);
    $widgetData = $widgetService->getWidgetData($data->type);

    $widget_title = $widgetData['title'];

    $tabs .= "<li class='widget_type'><span>$widget_title</span></li>";
    $tabs .= "<li><div class='area_tooltip noahs_page_builder-edit-widget' title='" .t('Edit')."' data-widget-id='$id'><i class='fa-solid fa-pen-to-square'></i></div></li>";
    $column_size = $data->column_size ?? NULL;
    if ($data->type === 'noahs_row') {
      $element = 'section';
      $widget_default = 'section';
      $tabs .= "<li><div class='noahs-add-column-section'>
      <div class='area_tooltip noahs-tool-add-section before' title='" .t('Add Section bebore')."' data-widget-id='$id'><i class='fa-solid fa-plus'></i></div>
      <div class='noahs-tool-add-column' data-bs-toggle='tooltip' data-bs-placement='right' title='" .t('Add Column')."' data-widget-id='$id'><i class='fa-solid fa-plus'></i></div>
      <div class='noahs-tool-add-section after 'data-bs-toggle='tooltip' data-bs-placement='bottom' title='" .t('Add Section after')."' data-widget-id='$id'><i class='fa-solid fa-plus'></i></div>
      <div></li>";
      $tabs .= "<li><div class='area_tooltip noahs_page_builder-move-section' title='" .t('Move')."' data-widget-id='$id'><i class='fa-solid fa-up-down-left-right'></i></div></li>";

    }
    elseif ($data->type === 'noahs_column') {
      $widget_default = 'column';
      $tabs .= "<li><div class='area_tooltip noahs_page_builder-add-element-widget' title='" .t('Add Widget')."' data-widget-id='$id'><i class='fa-solid fa-plus'></i></div></li>";
      $tabs .= "<li><div class='area_tooltip noahs_page_builder-move-column' title='" .t('Move')."' data-widget-id='$id'><i class='fa-solid fa-up-down-left-right'></i></div></li>";

    }
    else {
      $class[] = 'element-widget';
      $tabs .= "<li><div class='area_tooltip noahs_page_builder-move-widget' title='" .t('Move')."' data-widget-id='$id'><i class='fa-solid fa-up-down-left-right'></i></div></li>";
    }

    $class[] = 'scrollme';

    $tabs .= "<li><div class='area_tooltip noahs_page_builder-remove-widget' title='" .t('Remove')."' data-widget-id='$id'><i class='fa-solid fa-trash'></i></div></li>";
    $tabs .= "<li><div class='area_tooltip noahs_page_builder-up-widget' title='" .t('Up')."' data-widget-id='$id'><i class='fa-solid fa-arrow-up'></i></div></li>";
    $tabs .= "<li><div class='area_tooltip noahs_page_builder-down-widget' title='" .t('Down')."' data-widget-id='$id'><i class='fa-solid fa-arrow-down'></i></div></li>";
    if (empty($global_class)) {
      $tabs .= "<li><div class='area_tooltip noahs_page_builder-clone-widget' title='" .t('Clone')."' data-widget-id='$id'><i class='fa-solid fa-clone'></i></div></li>";
    }
    $tabs .= '<li><div class="dropdown">
      <div class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
        <i class="fa-solid fa-ellipsis-vertical"></i>
      </div>';
    $tabs .= '<ul class="dropdown-menu p-0" aria-labelledby="navbarDropdown">';
    if (empty($global_class)) {
      $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_copy_element">' . t('Copy') .'</div></li>';
    }
    if (is_module_installed('noahs_page_builder_pro')) {
      if ($data->type != 'noahs_row' && $data->type != 'noahs_column' && empty($global_class)) {
        $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_save_as_global">' . t('Save as Global') . '</div></li>';
        $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_save_as_default">' . t('Save/update as Default') . '</div></li>';
        $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_copy_style_element">' . t('Copy Styles') . '</div></li>';
        $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_paste_style_element">' . t('Paste Styles') . '</div></li>';
      }
      if (!empty($global_class)) {
        $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_remove_as_global">' . t('Detach as Global') . '</div></li>';
      }

      if ($data->type === 'noahs_row') {
        $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_save_as_theme">' . t('Save as Theme') . '</div></li>';
      }
    }
    if ($data->type === 'noahs_column' || $data->type === 'noahs_row') {
      $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_paste_element">' . t('Paste') . '</div></li>';
    }
    if ($data->type === 'noahs_row') {
      $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_paste_section" data-type="before">' . t('Paste before') . '</div></li>';
      $tabs .= '<li class="d-block"><div class="dropdown-item d-block noahs_paste_section" data-type="after">' . t('Paste after') . '</div></li>';
    }

    $tabs .= '</ul>
      </div></li>';
    $tabs .= '</ul>';
    $widget_class = '';

    $subclass = implode(' ', $subclass);
    $route = \Drupal::routeMatch()->getRouteName();

    // Default object when add new widget.
    if (empty($data->settings->wid)) {
      $data->settings->wid = !empty($data->wid) ? $data->wid : uniqid();
      $data->settings->noahs_id = $data->noahs_id;
      $data->settings->type = $data->type;
    }

    $obj->settings->element = !empty($data->settings->element) ? $data->settings->element : [];

    $html_tabs = '';
    $data_setting = '';
    $widgetStyles = '';
    $data_global = !empty($data->settings->global) ? 'data-widget-global="true"' : NULL;

    if ($route === 'noahs_page_builder.preview' ||
          $route === 'noahs_page_builder.widget' ||
          $route === 'noahs_page_builder.noahs_settings' ||
          $route === 'noahs_page_builder_pro.iframe' ||
          $route === 'noahs_page_builder.product_preview' ||
          $route === 'noahs_page_builder_pro.get_theme' ||
          $route === 'noahs_page_builder.regenerate_widget' ||
          $route === 'noahs_page_builder_pro.get_global_widget' ||
          $route === 'noahs_page_builder_pro.get_default_widget' ||
          $route === 'noahs_page_builder.final_widget'
          ) {

      $data_setting = !empty($data->settings) ? $data->settings : $obj;
      $data_setting = ' data-settings="' . htmlspecialchars(json_encode($data_setting), ENT_QUOTES, 'UTF-8') . '"';

      if (!empty($data->settings->element->cookie_control->text)) {
        $cookie = $data->settings->element->cookie_control->text;

        if (empty($_COOKIE[$cookie])) {
          $class[] = 'widget-with-cookie';
        }
      }
    }
    else {
      $tabs = '';
      $cookie = NULL;
      $current_request = \Drupal::request();

      if (!empty($data->settings->element->cookie_control->text)) {
        $cookie = $data->settings->element->cookie_control->text;

        if (empty($_COOKIE[$cookie])) {
          return '';
        }
      }
      if (!empty($data->settings->element->param_controls) && !empty($data->settings->element->param_controls_value)) {

        $param = $data->settings->element->param_controls->text;
        $expected_value = $data->settings->element->param_controls_value->text;
        $query_param_value = $current_request->query->get($param);
  
        if ($query_param_value != $expected_value) {
          return;
        }
      }

      if (!empty($data->settings->element->param_controls_default)){
        if ($current_request->query->has($data->settings->element->param_controls_default->text)){
      
          return;
        }
      }
    }

    if ($route === 'noahs_page_builder.noahs_settings_iframe') {
      $tabs = '';
    }
    // if (!empty($data->settings->element->if_token)) {
    //   $controlService = \Drupal::service('noahs_page_builder.control_service');
    //   $inlineCSS = new NoahsSaveStylesController($controlService);
    //   $efw = !empty($data->settings) ? json_decode(json_encode($data->settings), TRUE) : json_decode(json_encode($obj), TRUE);
    //   $widgetStyles = '<style id="w_style_' . $data->wid . '">' . $inlineCSS->generateWidgetStyles($efw) . '</style>';
    // }


    $class = implode(' ', $class);

    return '
          <' . $element . ' 
          class="' . $class . '" 
          data-widget-type="' . $widget_default . '" 
          id="widget-id-' . $id . '" 
          data-widget-id="' . $id . '" 
          data-widget-selector="' . 'widget-' . str_replace('_', '-', $data->type) . '" 
          ' . ($column_size ? 'data-column-size="' . $column_size . '"' : '') . '
          data-type="' . $data->type . '" 
          ' . $data_setting . '
          ' . $data_global . '
          >
          ' . $tabs . '
          ' . $divider_declaration . '
          <div class="' . $subclass . '">
                  ' . $content . '
          </div>
          </' . $element . '>
      ';
  }

  /**
   * Render Image.
   * */
  public function getMediaImage($mid, $style = 'original') {
    $site_name = \Drupal::config('system.site')->get('name');
    $image = '/' . NOAHS_PAGE_BUILDER_PATH . '/assets/img/widget-image.jpg';
    $alt = NULL;
    $title = NULL;
    $html = '';
    $current_user = \Drupal::currentUser();
    $has_permission = $current_user->hasPermission('administer noahs_page_builder');

    if ($mid) {
      $media = \Drupal::entityTypeManager()->getStorage('media')->load($mid);

      if ($media && $media->bundle() === 'image') {
        $media_field_name = 'field_media_image';

        if ($media->hasField($media_field_name) && !$media->get($media_field_name)->isEmpty()) {
          $file = $media->get($media_field_name)->entity;

          if ($file) {
            $file_uri = $file->getFileUri();

            $image = ($style === 'original')
                ? \Drupal::service('file_url_generator')->generateString($file_uri)
                : \Drupal::entityTypeManager()->getStorage('image_style')->load($style)->buildUrl($file_uri);

            $alt = $media->get($media_field_name)->alt ?? $site_name;
            $title = $media->get($media_field_name)->title ?? $site_name;
          }
        }
      }
    }

    $render = [
      '#theme' => 'image',
      '#uri' => $image,
      '#alt' => $alt,
      '#title' => $title,
      '#attributes' => [
        'class' => ['widget-image-src img-fluid'],
      ],
    ];

    if ($has_permission && isset($media)) {
      $url = Url::fromRoute('entity.media.edit_form', ['media' => $mid]);
      $render['#prefix'] = t('<a href="@link" class="noahs-contextual--link" title="' . t('Edit edia') . '" target="_blank"><i class="fas fa-edit"></i></a>', ['@link' => $url->toString()]);
    }

    $html .= \Drupal::service('renderer')->render($render);

    return $html;

  }

  public function getEntityUrl($data){

    $url = null;
    $entity_type = !empty($data->entity_type) ? $settings->entity_type : 'node';
    $entity = \Drupal::entityTypeManager()->getStorage('node')->load($entity_id);
  
    if ($entity) {
      $url = $entity->toUrl('canonical', ['relative' => TRUE])->toString();
    }

    return $url;
  }

}
