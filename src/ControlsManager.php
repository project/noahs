<?php

namespace Drupal\noahs_page_builder;

use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media;

/**
 * {@inheritdoc}
 */
class ControlsManager {

  /**
   * Render tabs with inputs iside.
   */
  public function renderTabs($tabs, $values, $multiple = NULL, $delta = NULL, $parent = NULL) {

    $controlService = \Drupal::service('noahs_page_builder.control_service');

    $html_tabs = '';
    $tab_titles = [];
    $items = [];

    $control_tabs = $controlService->getControlById('control_tabs');
    $control_tab = $controlService->getControlById('control_tab');

    if (!$multiple) {
      $array1 = $controlService->defaultFields();
      $array1 = $controlService->groupFields($array1);

      foreach ($array1 as $key => $value) {

        if (array_key_exists($key, $tabs)) {

          $tabs[$key]['items'] = array_merge(
          $tabs[$key]['items'],
          array_diff_key($value['items'], $tabs[$key]['items'])
          );

        }
        else {

          $tabs[$key] = $value;
        }
      }
    }

    $first = TRUE;

    foreach ($tabs as $tab_id => $tab) {
      $html_tab = '';

      // Controls items.
      foreach ($tab['items'] as $item_id => $item) {

        if (isset($item['type']) && $item['type'] === 'group') {

          $group = $controlService->getControlById('group');

          $html_group = '';
          foreach ($item['items'] as $group_item_id => $group_item) {

            $group_items = [
              'item' => $group_item,
              'wid' => $values ? $values['wid'] : NULL,
              'item_id' => $group_item_id,
              'delta' => $delta,
              'parent' => $parent,
              'values' => $values,
            ];
            $html_group .= $this->renderControls($group_items, $values['element'], 'group', $delta);
          }

          $html_tab .= $group->contentTemplate(
          [
            'content' => $html_group,
            'group_id' => $item_id,
            'title' => $item['title'],
            'open' => $item['open'] ?? NULL,
          ]
          );
        }
        else {
          $items[$item_id] = $item;
          $data = [
            'item' => $item,
            'wid' => $values['wid'] ?? NULL,
            'item_id' => $item_id,
            'delta' => $delta,
            'parent' => $parent,
            'values' => $values,
          ];

          $values_element = $values['element'] ?? [];

          $html_tab .= $this->renderControls($data, $values_element, '', $delta);
        }
      }

      $tab_titles[$tab_id] = $tab['title'];

      $html_tabs .= $control_tab->contentTemplate(
      [
        'content' => $html_tab,
        'tab_id' => $tab_id,
        'title' => $tab['title'],
        'delta' => $delta,
        'first' => $first,
      ]
      );

      $first = FALSE;
    }

    return [
      'form' => $control_tabs->contentTemplate([
        'content' => $html_tabs,
        'tabs' => $tab_titles,
        'delta' => $delta,
      ]),
    ];

  }

  /**
   * Render controls (inputs)
   */
  public function renderControls($data, $values, $wrapper = NULL, $delta = NULL) {
    $controlService = \Drupal::service('noahs_page_builder.control_service');
    return $controlService->extractHtml($data, $values, $wrapper, $delta);
  }

  /**
   * Get Classes from selector.
   */
  public function getClasses($items, $values, $wid) {

    foreach ($values as $item_id => $element) {
      if (!empty($element)) {
        if (!empty($items[$item_id]['style_selector']) && $items[$item_id]['style_selector'] === 'widget') {
          $class['#widget-id-' . $wid][] = $element;
        }
        elseif (!empty($items[$item_id]['style_selector'])) {
          $class['#widget-id-' . $wid . ' ' . $items[$item_id]['style_selector']][] = $element;
        }
      }
    }

    if (!empty($class)) {
      return $this->transformArray($class);
    }
  }

  /**
   * Get Attributes from selector.
   */
  public function getAttributes($items, $values, $wid) {
    $class = [];

    foreach ($values as $item_id => $element) {

      if (isset($element)) {
        if (!empty($items[$item_id]['style_type']) && $items[$item_id]['style_type'] === 'attribute') {
          if (!empty($element['text'])) {
            $element = $element['text'];
          }

          if ($items[$item_id]['style_selector'] === 'widget') {
            $class['#widget-id-' . $wid][$items[$item_id]['attribute_type']] = $element;
          }
          else {
            $class['#widget-id-' . $wid . ' ' . $items[$item_id]['style_selector']][$items[$item_id]['attribute_type']] = $element;
          }

        }
      }
    }

    return $class;

  }

  /**
   * Transform Array to implode.
   */
  public function transformArray($array) {
    $newArray = [];

    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $flattenedValues = [];

        foreach ($value as $val) {
          if (is_array($val)) {
            $flattenedValues[] = implode(' ', $val);
          }
          else {
            $flattenedValues[] = $val;
          }
        }

        $newArray[$key] = implode(' ', $flattenedValues);
      }
      else {
        $newArray[$key] = $value;
      }
    }

    return $newArray;
  }


}
